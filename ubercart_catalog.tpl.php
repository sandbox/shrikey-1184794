<?php
/* Available variables:
 * 
 * $catalog: Current term
 * $sub_categories => Child terms of current term
 * $products => Products within the current term. Number of products are defined in 'uc_product_nodes_per_page'
 * $pager => Pager of products.
 */
?>
<div id="catalog">
  <?php if($products): ?>
  <div id="products">
  <?php foreach($products as $product): ?>
    <div class="object">
      <?php if (isset($product->field_image)): ?>
      <div class="image">
        <?php print l(theme(
                'imagecache', 'uc_catalog_template', $product->field_image[0]['filepath'], 
                $product->title, $product->title), 
                'node/'.$product->nid, array('html' => fals)); ?>
      </div>
      <?php endif; ?>
      <div class="title">
        <?php print l($product->title, 'node/'.$product->nid); ?>
      </div>
      <div class="sell-price">
        <?php print uc_price($product->sell_price, array('subject'=>array('node'=>$product)), array('including-title' => false)); ?>
      </div>
    </div>
  <?php endforeach; ?>
  </div>
  <?php else: ?>
  <div id="sub-categories">
  <?php foreach($sub_categories as $cat): ?>
    <div class="object">
      <div class="image">
        <a href="<?php print base_path() . 'catalog/'.$cat->tid; ?>">
          <?php print theme('imagecache', 'uc_catalog_template', $cat->image['filepath'], $cat->name, $cat->name); ?>
        </a>
      </div>
      <div class="title"><?php print l($cat->name, 'catalog/'.$cat->tid); ?></div>
    </div>
  <?php endforeach; ?>
  </div>
  <?php endif; ?>
  <?php print $pager; ?>
</div>
